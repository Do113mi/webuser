package at.spengr.web;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import at.spengr.web.db.User;
import at.spengr.web.db.UserService;

@Controller
public class UserControl {
	
	private final UserService userService;
	
	@Autowired
	public UserControl(UserService userService) {
		this.userService = userService;
	}
	
	@RequestMapping(value="/users")
	public ModelAndView getUsers(){
		ModelAndView mav = new ModelAndView("users");
		
		ArrayList<String> l = new ArrayList<String>();
		Iterable<User> it = userService.findAll();
		int i =0;
		for (User user : it) {
			i++;
			l.add("User "+i+": "+user.getFirstname()+" "+user.getLastname());
			
			
		}
		mav.addObject("users", l);
		
		return mav;
	}
}
